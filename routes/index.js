const express = require('express');
const router = express.Router();

router.get('/', global.cache.route(), (req, res, next) => {
	res.render('index', {
		route: [{title: 'Home', location: '/'}],
		newsItems: [
			'An import calculator for Dutch consumer customs charges.',
			'A PostNL international tracking service, which tracks the package on foreign tracking-codes.',
			'A NOS teletext viewer, so you don\'t have to use their bloaty website.',
			'More to come soon...'
		]
	});
});

router.get('/privacy', global.cache.route(), (req, res, next) => {
	res.render('privacy', {
		route: [{ title: 'Privacy Policy', location: '/privacy' }]
	});
});

module.exports = router;
