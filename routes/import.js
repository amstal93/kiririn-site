const express = require('express');
const router = express.Router();


router.get('/', global.cache.route(), (req, res, next) => {
	let itemCost = req.query.itemCost;
	itemCost = itemCost ? itemCost : 0.00;

	let shippingCost = req.query.shippingCost;
	shippingCost = shippingCost ? shippingCost : 0.00;

	let shippingType = req.query.shippingType;
	shippingType = shippingType ? shippingType : 1;

	res.render('import', {
		route: [
			{title: 'Import', location: '/import'}
		],
		itemCost: itemCost,
		shippingCost: shippingCost,
		shippingType: shippingType
	});
});

module.exports = router;
